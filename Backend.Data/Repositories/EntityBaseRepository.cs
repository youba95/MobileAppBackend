﻿using System;
using System.Linq;
using System.Linq.Expressions;
using Backend.Data.Infrastructure;
using Backend.Entities;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;

namespace Backend.Data.Repositories
{
    public class EntityBaseRepository<T> : IEntityBaseRepository<T>
            where T : class, IEntityBase, new()
    {

        private MobileAppContext dataContext;

        #region Prperties
        protected IDbFactory DbFactory
        {
            get;
            private set;
        }

        protected MobileAppContext DbContext
        {
            get { return dataContext ?? (dataContext = DbFactory.Init()); }
        }

        public IQueryable<T> All
        {
            get
            {
                throw new NotImplementedException();
            }
        }

        #endregion

        public EntityBaseRepository(IDbFactory _dbFactory)
        {
            DbFactory = _dbFactory;
        }

        #region Implémentation de l'interface
        public virtual IQueryable<T> AllIncluding(params Expression<Func<T, object>>[] includeProperties)
        {
            IQueryable<T> query = DbContext.Set<T>();
            foreach (var includeProperty in includeProperties)
            {
                query = query.Include(includeProperty);
            }
            return query;
        }

        public virtual IQueryable<T> GetAll()
        {
            return DbContext.Set<T>();
        }

        public virtual T GetSingle(int id)
        {
            return GetAll().FirstOrDefault(x => x.ID == id);
        }

        public virtual IQueryable<T> FindBy(Expression<Func<T, bool>> predicate)
        {
            return DbContext.Set<T>().Where(predicate);
        }

        public virtual void Add(T entity)
        {
            DbEntityEntry dbEntityEntry = DbContext.Entry<T>(entity);
            DbContext.Set<T>().Add(entity);
        }

        public virtual void Delete(T entity)
        {
            DbEntityEntry dbEntityEntry = DbContext.Entry<T>(entity);
            dbEntityEntry.State = EntityState.Deleted;
        }

        public virtual void Edit(T entity)
        {
            DbEntityEntry dbEntityEntry = DbContext.Entry<T>(entity);
            dbEntityEntry.State = EntityState.Modified;
        } 
        #endregion
    }
}
