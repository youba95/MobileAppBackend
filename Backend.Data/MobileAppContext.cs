﻿
using Backend.Data.Configuration;
using Backend.Entities;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;

namespace Backend.Data
{
     public  class MobileAppContext : DbContext 
    {

        public MobileAppContext():base("MobileAppConnectionString")
        {
            Database.SetInitializer<MobileAppContext>(null);
        }

        #region Mes Tables

        public IDbSet<Agence> AgenceSet { get; set; }
        public IDbSet<Client> ClientSet { get; set; }
        public IDbSet<Compte> CompteSet { get; set; }
        public IDbSet<Mouvement> MouvementSet { get; set; }
        public IDbSet<User> UserSet { get; set; }
        public IDbSet<CommandeCarte> CommandeCarteSet { get; set; }
        public IDbSet<CommandeChequier> CommandeChequierSet { get; set; }
        //public IDbSet<Credit> CreditSet { get; set; }

        #endregion

        public virtual void Commit()
        {
            base.SaveChanges();
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();

            modelBuilder.Configurations.Add(new AgenceConfiguration());
            modelBuilder.Configurations.Add(new ClientConfiguration());
            modelBuilder.Configurations.Add(new CompteConfiguration());
            modelBuilder.Configurations.Add(new MouvementConfiguration());
            modelBuilder.Configurations.Add(new UserConfiguration());
            modelBuilder.Configurations.Add(new UserParticulierConfiguration());
            modelBuilder.Configurations.Add(new UserProfessionnelConfiguration());
            modelBuilder.Configurations.Add(new UserMoralConfiguration());
            modelBuilder.Configurations.Add(new CommandeChequierConfiguration());
            modelBuilder.Configurations.Add(new CommandeCarteConfiguration());

        }

    }
}
