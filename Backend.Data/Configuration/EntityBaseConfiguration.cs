﻿
using Backend.Entities;
using System.Data.Entity.ModelConfiguration;

namespace Backend.Data.Configuration
{
    public class EntityBaseConfiguration<T> : EntityTypeConfiguration<T> where T : class, IEntityBase 
    {
        public EntityBaseConfiguration()
        {
            HasKey(e => e.ID);
        }
    }
}
