﻿using Backend.Entities;

namespace Backend.Data.Configuration
{
    public class CommandeChequierConfiguration : EntityBaseConfiguration<CommandeChequier>
    {
        public CommandeChequierConfiguration()
        {
            Property(a => a.DateDemande).IsOptional();
            Property(a => a.type).IsOptional();
            Property(a => a.etat).IsOptional();
        }
    }

    public class CommandeCarteConfiguration : EntityBaseConfiguration<CommandeCarte>
    {
        public CommandeCarteConfiguration()
        {
            Property(a => a.DateDemande).IsOptional();
            Property(a => a.etat).IsOptional();
        }
    }

    public class AgenceConfiguration : EntityBaseConfiguration<Agence>
    {
        public AgenceConfiguration()
        {
            Property(a => a.AgenceId).IsRequired();
            Property(a => a.Libelle).IsRequired().HasMaxLength(200);
            Property(t => t.Telephone).IsRequired().HasMaxLength(10).IsFixedLength();
            Property(f => f.Fax).IsRequired().HasMaxLength(10).IsFixedLength();
            Property(v => v.Wilaya).IsRequired().HasMaxLength(30);
            Property(a => a.Adresse).IsRequired().HasMaxLength(200);
            Property(a => a.Email).IsRequired().HasMaxLength(50);
        }
    }

    
    public class ClientConfiguration : EntityBaseConfiguration<Client>
    {
        public ClientConfiguration()
        {
            Property(c => c.IdentifiantClient).IsRequired().HasMaxLength(10).IsFixedLength();
            Property(e => e.AgenceId).IsRequired();
            Property(u => u.UserId).IsRequired();
            Property(b => b.Bloque).IsOptional();
        }

    }

    public class CompteConfiguration : EntityBaseConfiguration<Compte>
    {
        public CompteConfiguration()
        {
            Property(n => n.CompteId).IsRequired().HasMaxLength(6).IsFixedLength();
            Property(c => c.ClientId).IsRequired();
            Property(t => t.Type).IsRequired().HasMaxLength(3);
            Property(s => s.Solde).IsRequired();
            Property(o => o.Opposition).IsOptional();
            Property(d => d.Date_Creation).IsRequired();
            Property(c => c.Cloture).IsRequired();
            Property(d => d.Date_Cloture);
            Property(m => m.Motif_Cloture).IsOptional();
            Property(r => r.RIB).IsRequired().HasMaxLength(20).IsFixedLength();
            Property(r => r.DateMaj).HasMaxLength(20);
            Property(r => r.commande).IsOptional();
        }
    }

    public class MouvementConfiguration : EntityBaseConfiguration<Mouvement>
    {
        public MouvementConfiguration()
        {
            Property(n => n.MouvementId).IsRequired().HasMaxLength(12);
            Property(c => c.CompteId).IsRequired();
            Property(m => m.Montant).IsRequired();
            Property(d => d.Date_Transaction).IsRequired();
            Property(c => c.Code_Operation).IsRequired().HasMaxLength(6).IsFixedLength();
            Property(s => s.Solde_Avant).IsRequired();
            Property(s => s.Solde_Nouveau).IsRequired();
        }
    }

    public class UserConfiguration : EntityBaseConfiguration<User>
    {
        public UserConfiguration()
        {
            Property(u => u.UserId).IsRequired();
            Property(t => t.Type).IsRequired().HasMaxLength(1);
            Property(t => t.Telephone).IsRequired().HasMaxLength(10);
            Property(p => p.password).IsRequired().HasMaxLength(200);
            Property(e => e.Email).IsRequired().HasMaxLength(100);
            Property(e => e.Adresse).IsRequired().HasMaxLength(200);

        }
    }

    public class UserParticulierConfiguration : EntityBaseConfiguration<UserParticulier>
    {
        public UserParticulierConfiguration()
        {
            Property(n => n.Nom).IsRequired().HasMaxLength(30);
            Property(p => p.Prenom).IsRequired().HasMaxLength(30);
            Property(d => d.Date_Naissance).IsRequired();
            Property(l => l.Lieu_Naissance).IsRequired().HasMaxLength(20);
            Property(s => s.Situation_Familiale).IsRequired().HasMaxLength(2).IsFixedLength();
            Property(s => s.Sexe).IsRequired().HasMaxLength(20);
        }
    }

    public class UserProfessionnelConfiguration : EntityBaseConfiguration<UserProfessionnel>
    {
        public UserProfessionnelConfiguration()
        {
            Property(n => n.Nom).IsRequired().HasMaxLength(30);
            Property(p => p.Prenom).IsRequired().HasMaxLength(30);
            Property(d => d.Date_Naissance).IsRequired();
            Property(l => l.Lieu_Naissance).IsRequired().HasMaxLength(20);
            Property(s => s.Situation_Familiale).IsRequired().HasMaxLength(2).IsFixedLength();
            Property(s => s.Sexe).IsRequired().HasMaxLength(20);
            Property(a => a.Activite).IsRequired().HasMaxLength(30);
            Property(a => a.NIF).IsRequired().HasMaxLength(30);
            Property(a => a.NIS).IsRequired().HasMaxLength(30);
        }
    }

    public class UserMoralConfiguration : EntityBaseConfiguration<UserMoral>
    {
        public UserMoralConfiguration()
        {
            Property(r => r.Raison_Social).IsRequired().HasMaxLength(50);
            Property(e => e.Email2).IsRequired().HasMaxLength(100);
            Property(m => m.password2).IsRequired().HasMaxLength(200);
            Property(a => a.NIF).IsRequired().HasMaxLength(30);
            Property(a => a.NIS).IsRequired().HasMaxLength(30);
            Property(a => a.Fax).IsRequired().HasMaxLength(10);
        }
    }

    //public class CreditConfiguration : EntityBaseConfiguration<Credit_B>
    //{
    //    public CreditConfiguration()
    //    {
    //        Property(m => m.CreditId).IsRequired();
    //        Property(m => m.Mensualite).IsRequired();
    //        Property(m => m.Duree).IsRequired();
    //        Property(m => m.MontantRestant).IsOptional();
    //        Property(d => d.DatePrelevement).IsRequired();
    //        Property(r => r.Retard);

    //    }
    //}

    public class lesCreditConfiguration : EntityBaseConfiguration<lesCredits>
    {
        public lesCreditConfiguration()
        {
            Property(m => m.CreditId).IsRequired();
            Property(m => m.Mensualite).IsRequired();
            Property(m => m.Duree).IsRequired();
            Property(m => m.MontantRestant).IsOptional();
            Property(d => d.DatePrelevement).IsRequired();
            Property(r => r.Retard);
        }
    }
}
