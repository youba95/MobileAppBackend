﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Backend.Data.Infrastructure
{
    public class DbFactory : Disposable, IDbFactory
    {
        MobileAppContext dbContext;


        public MobileAppContext Init()
        {
            return dbContext ?? (dbContext = new MobileAppContext()); 
        }

        protected override void DisposeCore()
        {
            if (dbContext != null)
                dbContext.Dispose();
        }
    }
}
