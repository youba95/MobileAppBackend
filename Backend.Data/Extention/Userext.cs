﻿using Backend.Data.Repositories;
using Backend.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Backend.Data.Extention
{
    public static class Userext
    {
        public static User FindUserByPass(this IEntityBaseRepository<User> userrepo, string username,string password)
        {
            return userrepo.GetAll().Where(x => x.UserId.Equals(username) && x.password.Equals(password)).SingleOrDefault();
        }
    }
}
