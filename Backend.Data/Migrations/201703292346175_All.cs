namespace Backend.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class All : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Agence",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Libelle = c.String(nullable: false, maxLength: 200),
                        Telephone = c.String(nullable: false, maxLength: 10, fixedLength: true),
                        Fax = c.String(nullable: false, maxLength: 10, fixedLength: true),
                        Ville = c.String(nullable: false, maxLength: 30),
                        Adresse = c.String(nullable: false, maxLength: 200),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.Client",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        IdentifiantClient = c.String(nullable: false, maxLength: 100),
                        AgenceId = c.Int(nullable: false),
                        UserId = c.Int(nullable: false),
                        Bloque = c.Boolean(),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Agence", t => t.AgenceId, cascadeDelete: true)
                .ForeignKey("dbo.User", t => t.UserId, cascadeDelete: true)
                .Index(t => t.AgenceId)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.Compte",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        CompteId = c.String(nullable: false, maxLength: 7, fixedLength: true),
                        Type = c.String(nullable: false, maxLength: 3),
                        Solde = c.Double(nullable: false),
                        Opposition = c.Boolean(),
                        Date_Creation = c.DateTime(nullable: false),
                        Cloture = c.Boolean(nullable: false),
                        Date_Cloture = c.DateTime(),
                        Motif_Cloture = c.String(),
                        RIB = c.String(nullable: false, maxLength: 20, fixedLength: true),
                        ClientId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Client", t => t.ClientId, cascadeDelete: true)
                .Index(t => t.ClientId);
            
            CreateTable(
                "dbo.Mouvement",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        MouvementId = c.String(nullable: false, maxLength: 12),
                        Montant = c.Double(nullable: false),
                        Date_Transaction = c.DateTime(nullable: false),
                        Code_Operation = c.String(nullable: false, maxLength: 6, fixedLength: true),
                        Solde_Avant = c.Double(nullable: false),
                        Solde_Nouveau = c.Double(nullable: false),
                        CompteId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Compte", t => t.CompteId, cascadeDelete: true)
                .Index(t => t.CompteId);
            
            CreateTable(
                "dbo.User",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        UserId = c.String(nullable: false),
                        Type = c.String(nullable: false, maxLength: 2),
                        Telephone = c.String(nullable: false, maxLength: 10),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.UserParticulier",
                c => new
                    {
                        ID = c.Int(nullable: false),
                        Nom = c.String(nullable: false, maxLength: 30),
                        Prenom = c.String(nullable: false, maxLength: 30),
                        Date_Naissance = c.DateTime(nullable: false),
                        Lieu_Naissance = c.String(nullable: false, maxLength: 20),
                        Situation_Familiale = c.String(nullable: false, maxLength: 2, fixedLength: true),
                        Email = c.String(nullable: false, maxLength: 30),
                        MotDePasse = c.String(nullable: false, maxLength: 200),
                        Sexe = c.String(nullable: false, maxLength: 2),
                        Adresse = c.String(nullable: false, maxLength: 100),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.User", t => t.ID)
                .Index(t => t.ID);
            
            CreateTable(
                "dbo.UserProfessionnel",
                c => new
                    {
                        ID = c.Int(nullable: false),
                        Nom = c.String(nullable: false, maxLength: 30),
                        Prenom = c.String(nullable: false, maxLength: 30),
                        Date_Naissance = c.DateTime(nullable: false),
                        Lieu_Naissance = c.String(nullable: false, maxLength: 20),
                        Situation_Familiale = c.String(nullable: false, maxLength: 2, fixedLength: true),
                        Email = c.String(nullable: false, maxLength: 30),
                        MotDePasse = c.String(nullable: false, maxLength: 200),
                        Sexe = c.String(nullable: false, maxLength: 2),
                        Adresse = c.String(nullable: false, maxLength: 100),
                        Activite = c.String(nullable: false, maxLength: 30),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.User", t => t.ID)
                .Index(t => t.ID);
            
            CreateTable(
                "dbo.UserMoral",
                c => new
                    {
                        ID = c.Int(nullable: false),
                        Raison_Social = c.String(nullable: false, maxLength: 50),
                        Fax = c.String(),
                        Email1 = c.String(nullable: false, maxLength: 30),
                        MotDePasse1 = c.String(nullable: false, maxLength: 200),
                        Email2 = c.String(nullable: false, maxLength: 30),
                        MotDePasse2 = c.String(nullable: false, maxLength: 200),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.User", t => t.ID)
                .Index(t => t.ID);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.UserMoral", "ID", "dbo.User");
            DropForeignKey("dbo.UserProfessionnel", "ID", "dbo.User");
            DropForeignKey("dbo.UserParticulier", "ID", "dbo.User");
            DropForeignKey("dbo.Client", "UserId", "dbo.User");
            DropForeignKey("dbo.Mouvement", "CompteId", "dbo.Compte");
            DropForeignKey("dbo.Compte", "ClientId", "dbo.Client");
            DropForeignKey("dbo.Client", "AgenceId", "dbo.Agence");
            DropIndex("dbo.UserMoral", new[] { "ID" });
            DropIndex("dbo.UserProfessionnel", new[] { "ID" });
            DropIndex("dbo.UserParticulier", new[] { "ID" });
            DropIndex("dbo.Mouvement", new[] { "CompteId" });
            DropIndex("dbo.Compte", new[] { "ClientId" });
            DropIndex("dbo.Client", new[] { "UserId" });
            DropIndex("dbo.Client", new[] { "AgenceId" });
            DropTable("dbo.UserMoral");
            DropTable("dbo.UserProfessionnel");
            DropTable("dbo.UserParticulier");
            DropTable("dbo.User");
            DropTable("dbo.Mouvement");
            DropTable("dbo.Compte");
            DropTable("dbo.Client");
            DropTable("dbo.Agence");
        }
    }
}
