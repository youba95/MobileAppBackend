namespace Backend.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addCommande : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.CommandeChequier",
                c => new
                {
                    ID = c.Int(nullable: false, identity: true),
                    type = c.String(nullable: true),
                    DateDemande = c.DateTime(nullable: true),
                    etat = c.Boolean(),
                    CompteId = c.Int(nullable: false),
                })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Compte", t => t.CompteId, cascadeDelete: true)
                .Index(t => t.CompteId);


            CreateTable(
                "dbo.CommandeCarte",
                c => new
                {
                    ID = c.Int(nullable: false, identity: true),
                    DateDemande = c.DateTime(nullable: true),
                    etat = c.Boolean(),
                    CompteId = c.Int(nullable: false),
                })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Compte", t => t.CompteId, cascadeDelete: true)
                .Index(t => t.CompteId);
        }
        
        public override void Down()
        {
            
        }
    }
}
