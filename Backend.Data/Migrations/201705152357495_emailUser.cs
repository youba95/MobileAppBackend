namespace Backend.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class emailUser : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Compte", "ClientId", "dbo.Client");
            DropIndex("dbo.Compte", new[] { "ClientId" });
            AddColumn("dbo.Compte", "Client_ID", c => c.Int());
            AddColumn("dbo.Credit", "MontantRestant", c => c.Double(nullable: false));
            AddColumn("dbo.User", "Email", c => c.String(nullable: false, maxLength: 100));
            AlterColumn("dbo.Client", "IdentifiantClient", c => c.String(nullable: false, maxLength: 10, fixedLength: true));
            AlterColumn("dbo.Compte", "CompteId", c => c.String(nullable: false, maxLength: 6, fixedLength: true));
            AlterColumn("dbo.Compte", "Date_Cloture", c => c.DateTime(nullable: false));
            AlterColumn("dbo.Compte", "ClientId", c => c.String(nullable: false));
            AlterColumn("dbo.UserMoral", "Email2", c => c.String(nullable: false, maxLength: 100));
            CreateIndex("dbo.Compte", "Client_ID");
            AddForeignKey("dbo.Compte", "Client_ID", "dbo.Client", "ID");
            DropColumn("dbo.UserMoral", "Email1");
            DropColumn("dbo.UserParticulier", "Email");
            DropColumn("dbo.UserProfessionnel", "Email");
        }
        
        public override void Down()
        {
            AddColumn("dbo.UserProfessionnel", "Email", c => c.String(nullable: false, maxLength: 30));
            AddColumn("dbo.UserParticulier", "Email", c => c.String(nullable: false, maxLength: 30));
            AddColumn("dbo.UserMoral", "Email1", c => c.String(nullable: false, maxLength: 30));
            DropForeignKey("dbo.Compte", "Client_ID", "dbo.Client");
            DropIndex("dbo.Compte", new[] { "Client_ID" });
            AlterColumn("dbo.UserMoral", "Email2", c => c.String(nullable: false, maxLength: 30));
            AlterColumn("dbo.Compte", "ClientId", c => c.Int(nullable: false));
            AlterColumn("dbo.Compte", "Date_Cloture", c => c.DateTime());
            AlterColumn("dbo.Compte", "CompteId", c => c.String(nullable: false, maxLength: 7, fixedLength: true));
            AlterColumn("dbo.Client", "IdentifiantClient", c => c.String(nullable: false, maxLength: 100));
            DropColumn("dbo.User", "Email");
            DropColumn("dbo.Credit", "MontantRestant");
            DropColumn("dbo.Compte", "Client_ID");
            CreateIndex("dbo.Compte", "ClientId");
            AddForeignKey("dbo.Compte", "ClientId", "dbo.Client", "ID", cascadeDelete: true);
        }
    }
}
