namespace Backend.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class aaaa : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.User", "Adresse", c => c.String(nullable: false, maxLength: 200));
            AddColumn("dbo.UserMoral", "NIF", c => c.String(nullable: false, maxLength: 30));
            AddColumn("dbo.UserMoral", "NIS", c => c.String(nullable: false, maxLength: 30));
            AddColumn("dbo.UserProfessionnel", "NIS", c => c.String(nullable: false, maxLength: 30));
            AlterColumn("dbo.User", "Type", c => c.String(nullable: false, maxLength: 1));
            AlterColumn("dbo.UserMoral", "Fax", c => c.String(nullable: false, maxLength: 10));
            AlterColumn("dbo.UserParticulier", "Sexe", c => c.String(nullable: false, maxLength: 20));
            AlterColumn("dbo.UserProfessionnel", "Sexe", c => c.String(nullable: false, maxLength: 20));
            DropColumn("dbo.UserParticulier", "Adresse");
            DropColumn("dbo.UserProfessionnel", "Adresse");
            DropColumn("dbo.UserProfessionnel", "NIC");
        }
        
        public override void Down()
        {
            AddColumn("dbo.UserProfessionnel", "NIC", c => c.String(nullable: false, maxLength: 30));
            AddColumn("dbo.UserProfessionnel", "Adresse", c => c.String(nullable: false, maxLength: 100));
            AddColumn("dbo.UserParticulier", "Adresse", c => c.String(nullable: false, maxLength: 100));
            AlterColumn("dbo.UserProfessionnel", "Sexe", c => c.String(nullable: false, maxLength: 2));
            AlterColumn("dbo.UserParticulier", "Sexe", c => c.String(nullable: false, maxLength: 2));
            AlterColumn("dbo.UserMoral", "Fax", c => c.String());
            AlterColumn("dbo.User", "Type", c => c.String(nullable: false, maxLength: 2));
            DropColumn("dbo.UserProfessionnel", "NIS");
            DropColumn("dbo.UserMoral", "NIS");
            DropColumn("dbo.UserMoral", "NIF");
            DropColumn("dbo.User", "Adresse");
        }
    }
}
