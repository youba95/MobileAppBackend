namespace Backend.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ajoutEmail : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Agence", "AgenceId", c => c.String(nullable: false, maxLength: 3, fixedLength: true));
            AddColumn("dbo.Agence", "Email", c => c.String(nullable: false, maxLength: 50));
            AddColumn("dbo.UserProfessionnel", "NIF", c => c.String(nullable: false, maxLength: 30));
            AddColumn("dbo.UserProfessionnel", "NIC", c => c.String(nullable: false, maxLength: 30));
        }
        
        public override void Down()
        {
            DropColumn("dbo.UserProfessionnel", "NIC");
            DropColumn("dbo.UserProfessionnel", "NIF");
            DropColumn("dbo.Agence", "Email");
            DropColumn("dbo.Agence", "AgenceId");
        }
    }
}
