namespace Backend.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class agenceint : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Agence", "AgenceId", c => c.Int(nullable: false));
        }
        
    }
}
