namespace Backend.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ajoutCredit : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Credit",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        NumCompte = c.String(),
                        Montant = c.Double(nullable: false),
                        Mensualite = c.Double(nullable: false),
                        MoisRestant = c.Int(nullable: false),
                        DatePrelevement = c.DateTime(nullable: false),
                        Retard = c.Boolean(nullable: false),
                        CompteId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Compte", t => t.CompteId, cascadeDelete: true)
                .Index(t => t.CompteId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Credit", "CompteId", "dbo.Compte");
            DropIndex("dbo.Credit", new[] { "CompteId" });
            DropTable("dbo.Credit");
        }
    }
}
