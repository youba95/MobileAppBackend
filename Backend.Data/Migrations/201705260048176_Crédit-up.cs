namespace Backend.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Créditup : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Credit", "CompteId", "dbo.Compte");
            DropIndex("dbo.Credit", new[] { "CompteId" });
            RenameColumn(table: "dbo.Credit", name: "CompteId", newName: "Compte_ID");
            DropPrimaryKey("dbo.Credit");
            AddColumn("dbo.Credit", "CreditId", c => c.String());
            AlterColumn("dbo.Credit", "ID", c => c.Int(nullable: false));
            AlterColumn("dbo.Credit", "Compte_ID", c => c.Int());
            AddPrimaryKey("dbo.Credit", "ID");
            CreateIndex("dbo.Credit", "ID");
            CreateIndex("dbo.Credit", "Compte_ID");
            AddForeignKey("dbo.Credit", "ID", "dbo.Compte", "ID");
            AddForeignKey("dbo.Credit", "Compte_ID", "dbo.Compte", "ID");
            DropColumn("dbo.Credit", "NumCompte");
            DropColumn("dbo.Credit", "Montant");
            DropColumn("dbo.Credit", "MoisRestant");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Credit", "MoisRestant", c => c.Int(nullable: false));
            AddColumn("dbo.Credit", "Montant", c => c.Double(nullable: false));
            AddColumn("dbo.Credit", "NumCompte", c => c.String());
            DropForeignKey("dbo.Credit", "Compte_ID", "dbo.Compte");
            DropForeignKey("dbo.Credit", "ID", "dbo.Compte");
            DropIndex("dbo.Credit", new[] { "Compte_ID" });
            DropIndex("dbo.Credit", new[] { "ID" });
            DropPrimaryKey("dbo.Credit");
            AlterColumn("dbo.Credit", "Compte_ID", c => c.Int(nullable: false));
            AlterColumn("dbo.Credit", "ID", c => c.Int(nullable: false, identity: true));
            DropColumn("dbo.Credit", "CreditId");
            AddPrimaryKey("dbo.Credit", "ID");
            RenameColumn(table: "dbo.Credit", name: "Compte_ID", newName: "CompteId");
            CreateIndex("dbo.Credit", "CompteId");
            AddForeignKey("dbo.Credit", "CompteId", "dbo.Compte", "ID", cascadeDelete: true);
        }
    }
}
