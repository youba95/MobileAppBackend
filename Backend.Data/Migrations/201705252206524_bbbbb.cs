namespace Backend.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class bbbbb : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Compte", "DateMaj", c => c.String(maxLength: 20));
            AddColumn("dbo.Compte", "commande", c => c.Boolean());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Compte", "commande");
            DropColumn("dbo.Compte", "DateMaj");
        }
    }
}
