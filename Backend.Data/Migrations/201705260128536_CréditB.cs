namespace Backend.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class CréditB : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Credit",
                c => new
                {
                    ID = c.Int(nullable: false),
                    CreditId = c.String(nullable: false, maxLength: 30),
                    MontantRestant = c.Double(nullable: false),
                    Mensualite = c.Double(nullable: false),
                    Duree = c.Int(nullable: false),
                    DatePrelevement = c.DateTime(nullable: false),
                    Retard = c.Boolean(nullable: false),
                    compteIdLiee = c.String(nullable: false, maxLength: 30),
                })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Compte", t => t.ID)
                .Index(t => t.ID);
        }
        
        public override void Down()
        {
            
        }
    }
}
