namespace Backend.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class credit_ajout : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Credit", "Duree", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Credit", "Duree");
        }
    }
}
