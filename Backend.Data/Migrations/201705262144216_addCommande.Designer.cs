// <auto-generated />
namespace Backend.Data.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.1.3-40302")]
    public sealed partial class addCommande : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(addCommande));
        
        string IMigrationMetadata.Id
        {
            get { return "201705262144216_addCommande"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
