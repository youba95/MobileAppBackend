namespace Backend.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Créditup3 : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Credit", "Compte_ID", "dbo.Compte");
            DropIndex("dbo.Credit", new[] { "Compte_ID" });
            AddColumn("dbo.Credit", "compteIdLiee", c => c.String());
            DropColumn("dbo.Credit", "Compte_ID");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Credit", "Compte_ID", c => c.Int());
            DropColumn("dbo.Credit", "compteIdLiee");
            CreateIndex("dbo.Credit", "Compte_ID");
            AddForeignKey("dbo.Credit", "Compte_ID", "dbo.Compte", "ID");
        }
    }
}
