namespace Backend.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class test2 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Agence", "Wilaya", c => c.String(nullable: false, maxLength: 30));
            AddColumn("dbo.User", "password", c => c.String(nullable: false, maxLength: 200));
            AddColumn("dbo.UserMoral", "password2", c => c.String(nullable: false, maxLength: 200));
            DropColumn("dbo.Agence", "Ville");
            DropColumn("dbo.UserMoral", "MotDePasse1");
            DropColumn("dbo.UserMoral", "MotDePasse2");
            DropColumn("dbo.UserParticulier", "MotDePasse");
            DropColumn("dbo.UserProfessionnel", "MotDePasse");
        }
        
        public override void Down()
        {
            AddColumn("dbo.UserProfessionnel", "MotDePasse", c => c.String(nullable: false, maxLength: 200));
            AddColumn("dbo.UserParticulier", "MotDePasse", c => c.String(nullable: false, maxLength: 200));
            AddColumn("dbo.UserMoral", "MotDePasse2", c => c.String(nullable: false, maxLength: 200));
            AddColumn("dbo.UserMoral", "MotDePasse1", c => c.String(nullable: false, maxLength: 200));
            AddColumn("dbo.Agence", "Ville", c => c.String(nullable: false, maxLength: 30));
            DropColumn("dbo.UserMoral", "password2");
            DropColumn("dbo.User", "password");
            DropColumn("dbo.Agence", "Wilaya");
        }
    }
}
