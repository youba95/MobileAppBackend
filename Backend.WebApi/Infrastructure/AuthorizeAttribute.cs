﻿using System.Web;

//When building an HTTP REST API, we should use appropriate HTTP response codes to indicate the status of a response.
//I always use 401 and 403 status code for getting authentication/authorization status. 401 (Unauthorized) - indicates
//that the request has not been applied because it lacks valid authentication credentials for the target resource.
//and 403 (Forbidden) - when the user is authenticated but isn’t authorized to perform the requested operation on the
//given resource.

//Unfortunately, the ASP.NET MVC/Web API [Authorize] attribute doesn’t behave that way – it always emits 401.
//So, here in our Web API application, I am going to add a class for override this behavior.Here we will return 403
//when the user is authenticated but not authorized to perform the requested operation.

namespace webApiTokenAuthentication
{
    public class AuthorizeAttribute : System.Web.Http.AuthorizeAttribute
    {
        protected override void HandleUnauthorizedRequest(System.Web.Http.Controllers.HttpActionContext actionContext)
        {
            if (!HttpContext.Current.User.Identity.IsAuthenticated)
            {
                base.HandleUnauthorizedRequest(actionContext);
            }
            else
            {
                actionContext.Response = new System.Net.Http.HttpResponseMessage(System.Net.HttpStatusCode.Forbidden);
            }
        }
    }
}