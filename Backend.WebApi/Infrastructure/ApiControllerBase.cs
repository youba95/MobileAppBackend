﻿using Backend.Data.Infrastructure;
using System;
using System.Data.Entity.Infrastructure;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Backend.WebApi.Infrastructure
{

    public class ApiControllerBase : ApiController
    {

        //protected readonly IEntityBaseRepository<Error> _errorsRepository;
        protected readonly IUnitOfWork _unitOfWork;



        protected ApiControllerBase(IUnitOfWork uow)
        {
            _unitOfWork = uow;

        }


        protected HttpResponseMessage CreateHttpResponse(HttpRequestMessage request, Func<HttpResponseMessage> function)
        {
            HttpResponseMessage response = null;

            try
            {
                response = function.Invoke();
            }
            catch (DbUpdateException ue)
            {
                //LogError(ue);
                response = request.CreateResponse(HttpStatusCode.BadRequest, ue.InnerException.Message);
            }
            catch (Exception ex)
            {
                //LogError(ex);
                response = request.CreateResponse(HttpStatusCode.InternalServerError, ex.Message);
            }

            return response;

        }

        //private void LogError(Exception e)
        //{
        //    throw new NotImplementedException();
        //}
    }
}