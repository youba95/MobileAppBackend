﻿using AutoMapper;
using Backend.Entities;
using Backend.WebApi.Models;
using System.Collections.Generic;

namespace Backend.WebApi.Infrastructure.Mapping
{
    public class DomainToViewModelMapping : Profile
    {
        public override string ProfileName
        {
            get
            {
                return "DomainToViewModelMapping";
            }
        }

        public DomainToViewModelMapping()
        {
            CreateMap<Agence, ModelView_AgenceDetail>();
            CreateMap<Agence, ModelView_AgenceDetail>();
            CreateMap<Compte, ModelView_Compte>()
                .ForMember(dest => dest.typeCompte, opt => opt.MapFrom(c => c.Type.Equals("220") ? "Chèque" : "Épargne"));
            CreateMap<Mouvement, ModelView_Mouvement>()
                .ForMember(dest => dest.date, opt => opt.MapFrom(c => c.Date_Transaction.Date.ToString("dd/MM/yyyy")))
                .ForMember(dest => dest.intitule, opt => opt.MapFrom(c => c.Code_Operation.StartsWith("110") ? "Virement" : "Paimement"));
            
        }
    }
}