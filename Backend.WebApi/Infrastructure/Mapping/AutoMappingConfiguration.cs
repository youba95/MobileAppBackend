﻿using AutoMapper;


namespace Backend.WebApi.Infrastructure.Mapping
{
    public static class AutoMappingConfiguration
    {
        public static MapperConfiguration MapperConfiguration;

        public static void RegisterMapping()
        {
            MapperConfiguration = new MapperConfiguration(cfg => cfg.AddProfile<DomainToViewModelMapping>());
        }
    }
}