﻿using Backend.Data.Infrastructure;
using Backend.Data.Repositories;
using Backend.Entities;
using Microsoft.Owin.Security.OAuth;
using System.Security.Claims;
using System.Threading.Tasks;
using Backend.Data.Extention;
using System.Web;

namespace webApiTokenAuthentication
{
    public class MyAuthorizationServerProvider : OAuthAuthorizationServerProvider
    {
        private readonly IEntityBaseRepository<User> _user;
        public MyAuthorizationServerProvider(IEntityBaseRepository<User> userRepo)
        {
            _user = userRepo;
        }
        public override async Task ValidateClientAuthentication(OAuthValidateClientAuthenticationContext context)
        {
            context.Validated(); // valider le client 
        }

        public override async Task GrantResourceOwnerCredentials(OAuthGrantResourceOwnerCredentialsContext context)
        {
            var user = _user.FindUserByPass(context.UserName,context.Password);
            if (user != null)
            {
                var identity = new ClaimsIdentity(context.Options.AuthenticationType);
                identity.AddClaim(new Claim("id", user.ID.ToString()));

                context.Validated(identity);
            }
            else
            {
                context.SetError("invalid_grant", "le nom d'utilisateur ou le mot de passe est incorrect.");
                return;
            }

        }
    }
}