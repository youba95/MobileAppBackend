﻿using Backend.Data.Infrastructure;
using Backend.Data.Repositories;
using Backend.Entities;
using Backend.WebApi.Infrastructure;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using AutoMapper;
using System.Linq;
using Backend.WebApi.Infrastructure.Mapping;
using System.Security.Claims;
using System;

namespace Backend.WebApi.Controllers
{
    [RoutePrefix("api/compte")]
    public class CompteController : ApiControllerBase
    {
        private List<Models.ModelView_Compte> CompteListeView = new List<Models.ModelView_Compte>();
        private readonly IEntityBaseRepository<Compte> _comptesRepository;
        private readonly IEntityBaseRepository<CommandeChequier> _commandeChequierRepository;
        private readonly IEntityBaseRepository<CommandeCarte> _commandeCarteRepository;
        private readonly IEntityBaseRepository<User> _userRepository;
        private readonly IMapper _mapper;
        public CompteController(IUnitOfWork uow, IEntityBaseRepository<User> userRepo, IEntityBaseRepository<Compte> comptesRepo, IEntityBaseRepository<CommandeCarte> commandeCarteRepo, IEntityBaseRepository<CommandeChequier> commandeChequierRepo) : base(uow)
        {
            _comptesRepository = comptesRepo;
            _commandeChequierRepository = commandeChequierRepo;
            _commandeCarteRepository = commandeCarteRepo;
            _userRepository = userRepo;

            if (_mapper == null)
            {
                _mapper = AutoMappingConfiguration.MapperConfiguration.CreateMapper();
            }
        }

        [Authorize]
        [HttpGet]
        public HttpResponseMessage Get(int AgenceId, HttpRequestMessage request)
        {
            return CreateHttpResponse(request, () =>
            {
                ClaimsPrincipal principal = Request.GetRequestContext().Principal as ClaimsPrincipal;
                int id = Int32.Parse(principal.Claims.Where(c => c.Type == "id").Single().Value);

                HttpResponseMessage response = null;

                List<Compte> clts = _comptesRepository.FindBy(c => c.Client.UserId.Equals(id) && c.Client.AgenceId.Equals(AgenceId) && c.Type != "204").ToList();

                CompteListeView = _mapper.Map<List<Compte>, List<Models.ModelView_Compte>>(clts);

                response = request.CreateResponse(HttpStatusCode.OK, CompteListeView);

                return response;
            });
        }

        [Authorize]
        [HttpPost]
        [Route("Carte")]
        public HttpResponseMessage commandeCarte(Models.CommandeCarte carte, HttpRequestMessage request)
        {
            ClaimsPrincipal principal = Request.GetRequestContext().Principal as ClaimsPrincipal;
            int id = Int32.Parse(principal.Claims.Where(c => c.Type == "id").Single().Value);
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                Compte c = _comptesRepository.GetSingle(carte.rib);
                User u = _userRepository.GetSingle(id);
                if (c.Client.UserId == id && u.password == carte.motDePasse)
                {
                    Backend.Entities.CommandeCarte comm = new Backend.Entities.CommandeCarte();
                    comm.etat = false;
                    comm.DateDemande = System.DateTime.Now;
                    comm.CompteId = carte.rib;
                    _commandeCarteRepository.Add(comm);
                    c.commande = true;
                    _unitOfWork.Commit();
                    response = request.CreateResponse(HttpStatusCode.OK, "Carte Commandé");
                    return response;
                }
                response = request.CreateResponse(HttpStatusCode.BadRequest, "Erreur");
                return response;
            });
        }

        [Authorize]
        [HttpPost]
        [Route("cheque")]
        public HttpResponseMessage commandecheque(Models.CommandeCarte carte, HttpRequestMessage request)
        {
            ClaimsPrincipal principal = Request.GetRequestContext().Principal as ClaimsPrincipal;
            int id = Int32.Parse(principal.Claims.Where(c => c.Type == "id").Single().Value);
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                Compte c = _comptesRepository.GetSingle(carte.rib);
                User u = _userRepository.GetSingle(id);
                if (c.Client.UserId == id && u.password == carte.motDePasse)
                {
                    Backend.Entities.CommandeChequier comm = new Backend.Entities.CommandeChequier();
                    comm.etat = false;
                    comm.DateDemande = System.DateTime.Now;
                    comm.CompteId = carte.rib;
                    comm.type = "25";
                    _commandeChequierRepository.Add(comm);
                    c.commande = true;
                    _unitOfWork.Commit();
                    response = request.CreateResponse(HttpStatusCode.OK, "cheque Commandé");
                    return response;
                }
                response = request.CreateResponse(HttpStatusCode.BadRequest, "Erreur");
                return response;
            });
        }
    }
}
