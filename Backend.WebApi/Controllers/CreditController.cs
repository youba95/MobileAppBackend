﻿using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using AutoMapper;
using Backend.Data.Infrastructure;
using Backend.WebApi.Infrastructure;
using Backend.Entities;
using Backend.Data.Repositories;
using System.Linq;
using Backend.WebApi.Infrastructure.Mapping;
using System.Security.Claims;
using System;

namespace Backend.WebApi.Controllers
{
    [RoutePrefix("api/Credit")]
    public class CreditController : ApiControllerBase
    {
        private IMapper _mapper;
        private readonly IEntityBaseRepository<lesCredits> _creditRepository;

        public CreditController(IUnitOfWork uow, IEntityBaseRepository<lesCredits> creditRepo) : base(uow)
        {
            _creditRepository = creditRepo;
            if (_mapper == null)
            {
                _mapper = AutoMappingConfiguration.MapperConfiguration.CreateMapper();
            }
        }

        [Authorize]
        [HttpGet]
        public HttpResponseMessage Get(HttpRequestMessage request)
        {
            return CreateHttpResponse(request, () =>
            {
                ClaimsPrincipal principal = Request.GetRequestContext().Principal as ClaimsPrincipal;
                int id = Int32.Parse(principal.Claims.Where(c => c.Type == "id").Single().Value);
                HttpResponseMessage response = null;

                List<lesCredits> clts = _creditRepository.FindBy(c => c.Client.UserId.Equals(id)).ToList();
                List<Models.ModelView_Credit> CreditListeView = new List<Models.ModelView_Credit>();

                foreach (lesCredits c in clts)
                {
                    string ret = c.Retard.Equals(false) ? "Aucun" : "";
                    CreditListeView.Add(new Models.ModelView_Credit(c.CreditId, c.CompteId, Math.Abs(c.Solde), Math.Abs(c.MontantRestant), c.DatePrelevement.Date.ToString("dd/MM/yyyy"), c.Retard));
                }

                response = request.CreateResponse(HttpStatusCode.OK, CreditListeView);

                return response;
            });
        }
    }
}