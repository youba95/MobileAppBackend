﻿using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using AutoMapper;
using Backend.Data.Infrastructure;
using Backend.WebApi.Infrastructure;
using Backend.Entities;
using Backend.Data.Repositories;
using System.Linq;
using Backend.WebApi.Infrastructure.Mapping;
using System.Security.Claims;
using System;

namespace Backend.WebApi.Controllers
{
    [RoutePrefix("api/AgenceClient")]
    public class AgenceClientController : ApiControllerBase
    {
        private List<Models.ModelView_AgenceDetail> AgenceListeView = new List<Models.ModelView_AgenceDetail>();
        private IMapper _mapper;
        private readonly IEntityBaseRepository<Agence> _agencesRepository;

        public AgenceClientController(IUnitOfWork uow,IEntityBaseRepository<Agence> agencesRepo) : base(uow)
        {
            _agencesRepository = agencesRepo;
            if (_mapper == null)
            {
                _mapper = AutoMappingConfiguration.MapperConfiguration.CreateMapper();
            }
        }

        [HttpGet]
        public HttpResponseMessage GetAgenceClient(int id,HttpRequestMessage request)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;

                List<Agence> liste = _agencesRepository.FindBy(e => e.Clients.Any(c=> c.UserId == id)).ToList();
                AgenceListeView = _mapper.Map<List<Agence>, List<Models.ModelView_AgenceDetail>>(liste);

                response = request.CreateResponse(HttpStatusCode.OK, AgenceListeView);

                return response;
            });
        }

        [Authorize]
        [HttpGet]
        public HttpResponseMessage GetAgence(HttpRequestMessage request)
        {
            ClaimsPrincipal principal = Request.GetRequestContext().Principal as ClaimsPrincipal;
            int id = Int32.Parse(principal.Claims.Where(c => c.Type == "id").Single().Value);
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;

                List<Agence> liste = _agencesRepository.FindBy(e => e.Clients.Any(c => c.UserId.Equals(id)) && e.Clients.Any(c => c.Comptes.Any(j => j.Type != "204"))).ToList();
                AgenceListeView = _mapper.Map<List<Agence>, List<Models.ModelView_AgenceDetail>>(liste);

                response = request.CreateResponse(HttpStatusCode.OK, AgenceListeView);

                return response;
            });
        }
    }
}