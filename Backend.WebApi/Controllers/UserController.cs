﻿using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using AutoMapper;
using Backend.Data.Infrastructure;
using Backend.WebApi.Infrastructure;
using Backend.Entities;
using Backend.Data.Repositories;
using System.Linq;
using Backend.WebApi.Infrastructure.Mapping;
using System.Security.Claims;
using System;
using Backend.WebApi.Models;
using System.Net.Mail;
using System.Text.RegularExpressions;

namespace Backend.WebApi.Controllers
{
    [RoutePrefix("api/User")]
    public class UserController : ApiControllerBase
    {
        private readonly IEntityBaseRepository<User> _userRepository;
        private readonly IEntityBaseRepository<UserParticulier> _userParticulierRepository;
        private readonly IEntityBaseRepository<UserProfessionnel> _userProfessionnelRepository;
        private readonly IEntityBaseRepository<UserMoral> _userMoralRepository;
        private readonly IMapper _mapper;

        public UserController(IUnitOfWork uow, IEntityBaseRepository<User> userRepo, IEntityBaseRepository<UserParticulier> userParticulier, IEntityBaseRepository<UserProfessionnel> userProfessionnel, IEntityBaseRepository<UserMoral> userMoral) : base(uow)
        {
            _userRepository = userRepo;
            _userParticulierRepository = userParticulier;
            _userProfessionnelRepository = userProfessionnel;
            _userMoralRepository = userMoral;
            if (_mapper == null)
            {
                _mapper = AutoMappingConfiguration.MapperConfiguration.CreateMapper();
            }
        }

        [Authorize]
        [HttpGet]
        public HttpResponseMessage GetProfil(HttpRequestMessage request)
        {
            ClaimsPrincipal principal = Request.GetRequestContext().Principal as ClaimsPrincipal;
            int id = Int32.Parse(principal.Claims.Where(c => c.Type == "id").Single().Value);

            return CreateHttpResponse(request, () =>
            {
                //HttpResponseMessage response = null;

                User u = _userRepository.GetSingle(id);

                int type = Int32.Parse(u.Type);

                switch(type)
                {
                    case 1://particulier
                        HttpResponseMessage response = null;
                        UserParticulier up = _userParticulierRepository.GetSingle(id);
                        Models.ModelView_UserParticulier mup = new Models.ModelView_UserParticulier(1, id.ToString(), up.Email, up.Telephone, up.Adresse, up.Nom, up.Prenom, up.Date_Naissance.Date.ToString(), up.Lieu_Naissance, up.Sexe); 
                        response = request.CreateResponse(HttpStatusCode.OK, mup);
                        return response;
                        break;
                    case 2://moral
                        UserMoral um = _userMoralRepository.GetSingle(id);
                        Models.ModelView_UserMoral mum = new Models.ModelView_UserMoral(2, id.ToString(), um.Email, um.Telephone, um.Adresse, um.Raison_Social, um.NIF, um.NIS);
                        response = request.CreateResponse(HttpStatusCode.OK, mum);
                        return response;
                        break;
                    case 3://professionnel
                        UserProfessionnel upr = _userProfessionnelRepository.GetSingle(id);
                        Models.ModelView_UserProfessionnel mupr = new Models.ModelView_UserProfessionnel(3, id.ToString(), upr.Email, upr.Telephone, upr.Adresse, upr.Nom, upr.Prenom, upr.Date_Naissance.Date.ToString(), upr.Lieu_Naissance, upr.Sexe, upr.NIF, upr.NIS);
                        response = request.CreateResponse(HttpStatusCode.OK, mupr);
                        return response;
                        break;
                }

                return null;
            });
        }

        [Authorize]
        [HttpPost]
        [Route("Tel")]
        public HttpResponseMessage ChangeTel(RequestChangementTelephone Tel, HttpRequestMessage request)
        {
            ClaimsPrincipal principal = Request.GetRequestContext().Principal as ClaimsPrincipal;
            int id = Int32.Parse(principal.Claims.Where(c => c.Type == "id").Single().Value);
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                User u = _userRepository.GetSingle(id);
                if(u.password == Tel.motDePasse)
                {
                    u.Telephone = Tel.information;
                    _userRepository.Edit(u);
                    _unitOfWork.Commit();
                    response = request.CreateResponse(HttpStatusCode.OK, "Numéro de Télephon changé");
                    return response;
                }
                response = request.CreateResponse(HttpStatusCode.BadRequest, "Erreur Mot de passe");
                return response;
            });

        }

        [Authorize]
        [HttpPost]
        [Route("mdp")]
        public HttpResponseMessage ChangeMDP(RequestChangementMotDePasse pass, HttpRequestMessage request)
        {
            ClaimsPrincipal principal = Request.GetRequestContext().Principal as ClaimsPrincipal;
            int id = Int32.Parse(principal.Claims.Where(c => c.Type == "id").Single().Value);
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                User u = _userRepository.GetSingle(id);
                if (u.password == pass.motDePasse && pass.information == pass.confirmationInformation)
                {
                    u.password = pass.information;
                    _userRepository.Edit(u);
                    _unitOfWork.Commit();
                    response = request.CreateResponse(HttpStatusCode.OK, "Mot de passe changé");
                    return response;
                }
                response = request.CreateResponse(HttpStatusCode.BadRequest, "Erreur Mot de passe");
                return response;
            });

        }

        [Authorize]
        [HttpPost]
        [Route("mail")]
        public HttpResponseMessage Changemail(RequestChangementEmail mail, HttpRequestMessage request)
        {
            ClaimsPrincipal principal = Request.GetRequestContext().Principal as ClaimsPrincipal;
            int id = Int32.Parse(principal.Claims.Where(c => c.Type == "id").Single().Value);
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                User u = _userRepository.GetSingle(id);
                if (u.password == mail.motDePasse)
                {
                    MailMessage mm = new MailMessage();
                    mm.To.Add(new MailAddress(mail.information, "Request for Verification"));
                    mm.From = new MailAddress("cnep@cnep.dz");
                    string urlee = "verification?email="+mail.information.Trim()+"&userid="+id;
                    mm.Body = "Bonjour <a href=http://192.168.1.5:5454/api/user/"+urlee.Trim()+" </a> Cliquer ici pour vérifier votre email";
                    mm.IsBodyHtml = true;
                    mm.Subject = "Verification Email Cnep-Banque";
                    SmtpClient smcl = new SmtpClient();
                    smcl.Host = "smtp.gmail.com";
                    smcl.Port = 587;
                    smcl.Credentials = new NetworkCredential("test2017cnep@gmail.com", "azerty123456789");
                    smcl.EnableSsl = true;
                    smcl.Send(mm);
                    response = request.CreateResponse(HttpStatusCode.OK, "Vérifier l'email");
                    return response;
                }
                response = request.CreateResponse(HttpStatusCode.BadRequest, "Erreur Mot de passe");
                return response;
            });

        }

        [HttpGet]
        [Route("verification")]
        public HttpResponseMessage mail(string email,string userid, HttpRequestMessage request)
        {
            
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                User u = null;
                u = _userRepository.GetSingle(Int32.Parse(userid));
                if (u != null)
                {
                    u.Email = email;
                    _userRepository.Edit(u);
                    _unitOfWork.Commit();
                    response = request.CreateResponse(HttpStatusCode.OK, "email vérifié");
                    return response;
                }
                response = request.CreateResponse(HttpStatusCode.BadRequest, "Erreur utilisateur");
                return response;
            });

        }
    }
}