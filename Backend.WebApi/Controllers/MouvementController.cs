﻿using Backend.Data.Infrastructure;
using Backend.Data.Repositories;
using Backend.Entities;
using Backend.WebApi.Infrastructure;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using AutoMapper;
using System.Linq;
using Backend.WebApi.Infrastructure.Mapping;

namespace Backend.WebApi.Controllers
{
    [RoutePrefix("api/mouvement")]
    public class MouvementController : ApiControllerBase
    {
        private List<Models.ModelView_Mouvement> MouvementListeView = new List<Models.ModelView_Mouvement>();
        private readonly IEntityBaseRepository<Mouvement> _mouvementRepository;
        private readonly IMapper _mapper;
        public MouvementController(IUnitOfWork uow, IEntityBaseRepository<Mouvement> mouvementRepo) : base(uow)
        {
            _mouvementRepository = mouvementRepo;

            if (_mapper == null)
            {
                _mapper = AutoMappingConfiguration.MapperConfiguration.CreateMapper();
            }
        }

        [Authorize]
        [HttpGet]
        public HttpResponseMessage GetMouvement(int idCompte, HttpRequestMessage request)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;

                List<Mouvement> clts = _mouvementRepository.FindBy(c => c.CompteId == idCompte).ToList();

                MouvementListeView = _mapper.Map<List<Mouvement>, List<Models.ModelView_Mouvement>>(clts);

                response = request.CreateResponse(HttpStatusCode.OK, MouvementListeView);

                return response;
            });
        }
    }
}