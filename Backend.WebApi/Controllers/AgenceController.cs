﻿using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using AutoMapper;
using Backend.Data.Infrastructure;
using Backend.WebApi.Infrastructure;
using Backend.Entities;
using Backend.Data.Repositories;
using System.Linq;
using Backend.WebApi.Infrastructure.Mapping;

namespace Backend.WebApi.Controllers
{
    [RoutePrefix("api/Agence")]
    public class AgenceController : ApiControllerBase
    {
        private List<Models.ModelView_AgenceDetail> AgenceListeView = new List<Models.ModelView_AgenceDetail>();
        private IMapper _mapper;
        private readonly IEntityBaseRepository<Agence> _agencesRepository;

        public AgenceController(IUnitOfWork uow, IEntityBaseRepository<Agence> agencesRepo) : base(uow)
        {
            _agencesRepository = agencesRepo;
            if (_mapper == null)
            {
                _mapper = AutoMappingConfiguration.MapperConfiguration.CreateMapper();
            }
        }

        [HttpGet]
        [Route("wilaya/{w}")]
        public HttpResponseMessage GetAgenceWilaya(string w, HttpRequestMessage request)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;

                List<Agence> clts = _agencesRepository.FindBy(c => c.Wilaya == w).ToList();

                AgenceListeView = _mapper.Map<List<Agence>, List<Models.ModelView_AgenceDetail>>(clts);

                response = request.CreateResponse(HttpStatusCode.OK, AgenceListeView);

                return response;
            });
        }
        [Route("{id}")]
        public HttpResponseMessage GetAgence(int id, HttpRequestMessage request)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;

                Agence clts = _agencesRepository.GetSingle(id);

                Models.ModelView_AgenceDetail agence = _mapper.Map<Agence, Models.ModelView_AgenceDetail>(clts);

                response = request.CreateResponse(HttpStatusCode.OK, agence);

                return response;
            });
        }

        [HttpGet]
        [Route("All")]
        public HttpResponseMessage GetAgenceAll(HttpRequestMessage request)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;

                List<Agence> clts = _agencesRepository.GetAll().ToList();

                AgenceListeView = _mapper.Map<List<Agence>, List<Models.ModelView_AgenceDetail>>(clts);

                response = request.CreateResponse(HttpStatusCode.OK, AgenceListeView);

                return response;
            });
        }

    }
}