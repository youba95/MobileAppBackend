﻿using System;
using System.Threading.Tasks;
using Microsoft.Owin;
using Owin;
using Microsoft.Owin.Security.OAuth;
using System.Web.Http;
using Backend.WebApi;
using Backend.Data.Repositories;
using Backend.Entities;
using Backend.Data.Infrastructure;

[assembly: OwinStartup(typeof(webApiTokenAuthentication.Startup))]

namespace webApiTokenAuthentication
{
    public class Startup
    {
        private readonly IEntityBaseRepository<User> _user;
        public Startup(IEntityBaseRepository<User> userrepo)
        {
            _user = userrepo;
        }

        public Startup()
        {
            _user = new EntityBaseRepository<User>(new DbFactory());
        }

        public void Configuration(IAppBuilder app)
        {
            // For more information on how to configure your application, visit http://go.microsoft.com/fwlink/?LinkID=316888
            //enable cors origin requests
            app.UseCors(Microsoft.Owin.Cors.CorsOptions.AllowAll);
            var myProvider = new MyAuthorizationServerProvider(_user);
            OAuthAuthorizationServerOptions options = new OAuthAuthorizationServerOptions
            {
                AllowInsecureHttp = true,
                TokenEndpointPath = new PathString("/token"),
                AccessTokenExpireTimeSpan = TimeSpan.FromMinutes(10),
                Provider = myProvider
            };
            app.UseOAuthAuthorizationServer(options);
            app.UseOAuthBearerAuthentication(new OAuthBearerAuthenticationOptions());

            HttpConfiguration config = new HttpConfiguration();
            WebApiConfig.Register(config);
        }
    }
}