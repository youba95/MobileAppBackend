﻿
namespace Backend.WebApi.Models
{
    public class RequestChangementEmail
    {
        public string information { get; set; }
        public string motDePasse { get; set; }
        public string typeRequete { get; set; }
    }
}