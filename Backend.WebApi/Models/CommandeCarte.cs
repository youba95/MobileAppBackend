﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Backend.WebApi.Models
{
    public class CommandeCarte
    {
        public CommandeCarte(int rib, string motDePasse, int type)
        {
            this.rib = rib;
            this.motDePasse = motDePasse;
            this.type = type;
        }

        public int rib { get; set; }
        public string motDePasse { get; set; }
        public int type { get; set; }
    }
}