﻿
namespace Backend.WebApi.Models
{
    public class RequestChangementMotDePasse
    {
        public string information { get; set; }
        public string motDePasse { get; set; }
        public string typeRequete { get; set; }
        public string confirmationInformation { get; set; }
    }
}