﻿namespace Backend.WebApi.Models
{
    public class ModelView_Compte
    {
        public int id { get; set; }
        public string compteId { get; set; }
        public string typeCompte { get; set; }
        public double solde { get; set; }
        public string rib { get; set; }
        public string DateMaj { get; set; }
        public bool commande { get; set; }
    }
}