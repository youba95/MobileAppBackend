﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Backend.WebApi.Models
{
    public class ModelView_AgenceDetail
    {
        public int id { get; set; }
        public int agenceid { get; set; }
        public string libelle { get; set; }
        public string wilaya { get; set; }
        public string adresse { get; set; }
        public string telephone { get; set; }
        public string email { get; set; }
    }
}