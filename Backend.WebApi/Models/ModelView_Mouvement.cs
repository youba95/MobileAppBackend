﻿using System;

namespace Backend.WebApi.Models
{
    public class ModelView_Mouvement
    {
        public string date { get; set; }
        public string intitule { get; set; }
        public double montant { get; set; }
    }
}