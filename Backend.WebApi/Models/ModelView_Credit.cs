﻿using System;

namespace Backend.WebApi.Models
{
    public class ModelView_Credit
    {
        public ModelView_Credit(string idCrédit, string compteId, double montantAcordé, double montantRestant, string datePrelevement, bool retard)
        {
            this.idCrédit = idCrédit;
            this.compteId = compteId;
            this.montantAcordé = montantAcordé;
            this.montantRestant = montantRestant;
            this.datePrelevement = datePrelevement;
            this.retard = retard;
        }

        public string idCrédit { get; set; }
        public string compteId { get; set; }
        public double montantAcordé { get; set; }
        public double montantRestant { get; set; }
        public string datePrelevement { get; set; }
        public bool retard { get; set; }
    }
}