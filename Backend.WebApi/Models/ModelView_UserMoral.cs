﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Backend.WebApi.Models
{
    public class ModelView_UserMoral
    {
        public ModelView_UserMoral(int type, string userid, string email, string telephone, string adresse, string raison_Social, string nif, string nis)
        {
            this.type = type;
            this.userid = userid;
            this.email = email;
            this.telephone = telephone;
            this.adresse = adresse;
            Raison_Social = raison_Social;
            this.nif = nif;
            this.nis = nis;
        }

        public int type { get; set; }
        public string userid { get; set; }
        public string email { get; set; }
        public string telephone { get; set; }
        public string adresse { get; set; }
        public string Raison_Social { get; set; }
        public string nif { get; set; }
        public string nis { get; set; }

    }
}