﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Backend.WebApi.Models
{
    public class ModelView_UserParticulier
    {
        public ModelView_UserParticulier(int type, string userid, string email, string telephone, string adresse, string nom, string prenom, string date_Naissance, string lieu_Naissance, string sex)
        {
            this.type = type;
            this.userid = userid;
            this.email = email;
            this.telephone = telephone;
            this.adresse = adresse;
            this.nom = nom;
            this.prenom = prenom;
            this.date_Naissance = date_Naissance;
            this.lieu_Naissance = lieu_Naissance;
            this.sex = sex;
        }

        public int type { get; set; }
        public string userid { get; set; }
        public string email { get; set; }
        public string telephone { get; set; }
        public string adresse { get; set; }
        public string nom { get; set; }
        public string prenom { get; set; }
        public string date_Naissance { get; set; }
        public string lieu_Naissance { get; set; }
        public string sex { get; set; }


    }
}