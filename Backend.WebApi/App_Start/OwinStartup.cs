﻿//using Backend.Data.Repositories;
//using Backend.Entities;
//using Backend.WebApi.App_Start;
//using Microsoft.Owin;
//using Microsoft.Owin.Security.OAuth;
//using Owin;
//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Web;
//using System.Web.Http;
//using webApiTokenAuthentication;

//[assembly: OwinStartup(typeof(OwinStartup))]
//namespace Backend.WebApi.App_Start
//{
//    public class OwinStartup
//    {
//        private readonly IEntityBaseRepository<User> _user;
//        public OwinStartup(IEntityBaseRepository<User> userrepo)
//        {
//            _user = userrepo;
//        }

//        public OwinStartup()
//        {
            
//        }

//        public void Configuration(IAppBuilder app)
//        {
//            // For more information on how to configure your application, visit http://go.microsoft.com/fwlink/?LinkID=316888
//            //enable cors origin requests
//            app.UseCors(Microsoft.Owin.Cors.CorsOptions.AllowAll);

//            var myProvider = new MyAuthorizationServerProvider(_user);
//            OAuthAuthorizationServerOptions options = new OAuthAuthorizationServerOptions
//            {
//                AllowInsecureHttp = true,
//                TokenEndpointPath = new PathString("/token"),
//                AccessTokenExpireTimeSpan = TimeSpan.FromMinutes(1),
//                Provider = myProvider
//            };
//            app.UseOAuthAuthorizationServer(options);
//            app.UseOAuthBearerAuthentication(new OAuthBearerAuthenticationOptions());


//            HttpConfiguration config = new HttpConfiguration();
//            WebApiConfig.Register(config);
//        }
//    }
//}