﻿using System.Web.Http;

namespace Backend.WebApi
{
    public class Bootstrapper
    {
        public static void  Run()
        {
            // Configure IoC Autofac
            AutofacWebApiConfig.Initialize(GlobalConfiguration.Configuration);
            //AutoMapper
            Infrastructure.Mapping.AutoMappingConfiguration.RegisterMapping();
            //OwinStartup
        }
    }
}