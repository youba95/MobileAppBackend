﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace Backend.Entities
{
    [Table("UserProfessionnel")]
    public class UserProfessionnel : User
    {
        public string Nom { get; set; }
        public string Prenom { get; set; }
        public DateTime Date_Naissance { get; set; }
        public string Lieu_Naissance { get; set; }
        public string Situation_Familiale { get; set; }
        public string Sexe { get; set; }
        public string Activite { get; set; }
        public string NIF { get; set; }
        public string NIS { get; set; }
    }
}
