﻿namespace Backend.Entities
{
    public interface IEntityBase
    {
        int ID { get; set; }
    }
}