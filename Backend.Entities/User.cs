﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Backend.Entities
{
    [Table("User")]
    public class User : IEntityBase
    {
        public int ID { get; set; }
        public string UserId { get; set; }
        public string Type { get; set; }
        public string Telephone { get; set; }
        public string password { get; set; }
        public string Email { get; set; }
        public string Adresse { get; set; }

        public virtual ICollection<Client> Clients { get; set; }
        public User()
        {
            Clients = new List<Client>();
        }
    }
}