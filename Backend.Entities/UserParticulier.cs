﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace Backend.Entities
{
    [Table("UserParticulier")]
    public class UserParticulier : User
    {
        public string Nom { get; set; }
        public string Prenom { get; set; }
        public DateTime Date_Naissance { get; set; }
        public string Lieu_Naissance { get; set; }
        public string Situation_Familiale { get; set; }
        public string Sexe { get; set; }

    }
}
