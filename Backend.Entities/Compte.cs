﻿using System;
using System.Collections.Generic;

namespace Backend.Entities
{
    public class Compte : IEntityBase
    {
        public int ID { get; set; }
        public string CompteId { get; set; }
        public string Type { get; set; }
        public double Solde { get; set; }
        public bool Opposition { get; set; }
        public DateTime Date_Creation { get; set; }
        public bool Cloture { get; set; }
        public DateTime Date_Cloture { get; set; }
        public string Motif_Cloture { get; set; }
        public string RIB { get; set; }
        public string DateMaj { get; set; }
        public bool commande { get; set; }

        public string ClientId { get; set; }
        public virtual Client Client { get; set; }

        public virtual ICollection<Mouvement> Mouvements { get; set; }
        public Compte()
        {
            Mouvements = new List<Mouvement>();
        }
}
}
