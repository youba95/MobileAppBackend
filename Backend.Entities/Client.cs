﻿using System.Collections.Generic;

namespace Backend.Entities
{
    public class Client : IEntityBase
    {
        public int ID  { get;  set;}
        public string IdentifiantClient { get; set; }
        public int AgenceId { get; set; }
        public int UserId { get; set; }
        public bool Bloque { get; set; }

        public virtual Agence Agence { get; set; }

        public virtual ICollection<Compte> Comptes { get; set; }
        public Client()
        {
            Comptes = new List<Compte>();
        }

    }
}