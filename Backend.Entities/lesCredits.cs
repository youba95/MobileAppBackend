﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Backend.Entities
{
    [Table("Credit")]
    public class lesCredits : Compte
    {
        public string CreditId { get; set; }
        public double MontantRestant { get; set; }
        public double Mensualite { get; set; }
        public int Duree { get; set; }
        public DateTime DatePrelevement { get; set; }
        public bool Retard { get; set; }
        public string compteIdLiee { get; set; }
    }
}
