﻿using System.ComponentModel.DataAnnotations.Schema;

namespace Backend.Entities
{
    [Table("UserMoral")]
    public class UserMoral : User
    {
        public string Raison_Social { get; set; }
        public string NIF { get; set; }
        public string NIS { get; set; }
        public string Fax { get; set; }
        public string Email2 { get; set; }
        public string password2 { get; set; }
    }
}
