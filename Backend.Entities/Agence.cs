﻿using System.Collections.Generic;

namespace Backend.Entities
{
    public class Agence : IEntityBase
    {
        public int ID { get; set; }
        public int AgenceId { get; set; }
        public string Libelle { get; set; }
        public string Telephone { get; set; }
        public string Fax { get; set; }
        public string Wilaya { get; set; }
        public string Adresse { get; set; }
        public string Email { get; set; }

    public virtual ICollection<Client> Clients { get; set; }

        public Agence()
        {
            Clients = new List<Client>();
        }
    }
}

  