﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Backend.Entities
{
    public class CommandeChequier : IEntityBase
    {
        public int ID { get; set; }
        public string type { get; set; }
        public DateTime DateDemande { get; set; }
        public bool etat { get; set; }

        public int CompteId { get; set; }
        public virtual Compte Compte { get; set; }
    }
}
