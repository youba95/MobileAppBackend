﻿using System;

namespace Backend.Entities
{
    public class Mouvement : IEntityBase
    {
        public int ID { get; set; }
        public string MouvementId { get; set; }
        public double Montant { get; set; }
        public DateTime Date_Transaction { get; set; }
        public string Code_Operation { get; set; }
        public double Solde_Avant { get; set; }
        public double Solde_Nouveau { get; set; }

        public int CompteId { get; set; }
        public virtual Compte Compte { get; set; }

}
}
